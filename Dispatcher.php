<?php


class Dispatcher
{

	/**
	 * @var IGrabber
	 */
	private $grabber;

	/**
	 * @var IOutput
	 */
	private $output;

	/**
	 * @param IGrabber $grabber
	 * @param IOutput $output
	 */
	public function __construct(IGrabber $grabber, IOutput $output)
	{
		$this->grabber = $grabber;
		$this->output = $output;
	}

	/**
	 * @return string JSON
	 */
	public function run()
	{
	    $filename = 'vstup.txt';

        $handle = fopen($filename, 'r');
	    if (!$handle) {
            throw new \ErrorException('Nepodařilo se otevřít soubor "' . $filename . '"');
        }

        while (($productId = fgets($handle)) !== false) {
	        $productId = trim($productId);
	        try {
                $this->grabber->getPrice($productId);
            } catch (NotFoundException $e) {}
        }

	    fclose($handle);

	    return $this->output->getJson();
	}
}
