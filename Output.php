<?php

class Output implements IOutput
{
    /**
     * @var Storage
     */
    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * {@inheritDoc}
     */
    public function getJson()
    {
        return json_encode($this->storage->getProducts());
    }
}
