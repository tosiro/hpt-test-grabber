<?php


interface IOutput
{

	/**
	 * @return string
	 */
	public function getJson();

}
