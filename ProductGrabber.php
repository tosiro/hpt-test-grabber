<?php

class ProductGrabber implements IGrabber
{
    /**
     * @var Storage
     */
    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * {@inheritDoc}
     */
    public function getPrice($productId)
    {
         return $this->storage->getProduct($productId)->getPrice();
    }
}
