<?php

class ProductEntity implements JsonSerializable
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;

    /**
     * @var int
     */
    private $rating;

    /**
     * ProductEntity constructor.
     * @param string $location
     * @param string $id
     */
    public function __construct($location, $id)
    {
        $this->location = $location;
        $this->id       = $id;
        $this->initialize();
    }

    private function initialize()
    {
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTMLFile($this->location); // urlencode()
        $dom->saveHTMLFile('dumps/detail_' . date('Y-m-d_H-i-s') . '.html');
        $xpath = new DOMXPath($dom);

        # name
        $name = $xpath->query('//h1')->item(0)->nodeValue;
        $this->name = (string) trim($name);

        # price
        $price = $xpath
            ->query('//div[@class="total-price"]/span[contains(@class, "price ")]/span[@class="price-vatin"]')
            ->item(0)
            ->nodeValue;
        $this->price = (float) hex2bin(str_replace('c2a0', '', bin2hex($price)));

        # $rating
        $rating = $xpath->query('//span[@class="rating__label"]')->item(0);
        $rating = $rating ? $rating->nodeValue : 0;
        $this->rating = (int) $rating;
    }

    public function jsonSerialize()
    {
        return [
            'name'   => $this->getName(),
            'price'  => $this->getPrice(),
            'rating' => $this->getRating(),
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }
}
