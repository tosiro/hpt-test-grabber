<?php

class Storage
{
    const WWW_PATH = 'https://www.czc.cz';

    private static $searchPath = self::WWW_PATH . '/%s/hledat';

    /**
     * @var ProductEntity[]
     */
    private $storedProducts;

    /**
     * @param $id
     * @return ProductEntity
     * @throws NotFoundException
     */
    public function getProduct($id)
    {
        if (isset($this->storedProducts[$id])) {
            return $this->storedProducts[$id];
        }

        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTMLFile(sprintf(self::$searchPath, rawurlencode($id))); // urlencode()
        $dom->saveHTMLFile('dumps/search_' . date('Y-m-d_H-i-s') . '.html');
        $xpath = new DOMXPath($dom);
        $nodes = $xpath->query('//div[@id="tiles"]/*/div[contains(@class, "new-tile")]');
        if (!$nodes || !count($nodes)) {
            throw new NotFoundException('Nepodřailo se dohledat produkt "' . $id . '".');
        }

        $a = $xpath->query('./a', $nodes->item(0));
        $href = (string) $a->item(0)->attributes->getNamedItem('href')->nodeValue;
        $href = self::WWW_PATH . $href;

        return $this->storedProducts[$id] = new ProductEntity($href, $id);
    }

    /**
     * @return ProductEntity[]
     */
    public function getProducts()
    {
        return $this->storedProducts;
    }
}
