<?php

spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

$storage = new Storage();
$iGrabber = new ProductGrabber($storage);
$iOutput = new Output($storage);

// code here
$dispatcher = new Dispatcher(
    $iGrabber, $iOutput
);
echo $dispatcher->run();
